# This file is part of mockrtdb.

# mockrtdb is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# mockrtdb is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# mockrtdb. If not, see <http://www.gnu.org/licenses/>.

import configparser

# This is going to hold our settings for as long as the app runs. It shouldn't
# be modified directly, and its contents should only be altered through the use
# of a configuration file, or command line arguments.
settings = {
    'db': {
        'standard_items': 0,
        'realtime_items': 0,
        'items_min_lifetime': 0,
        'items_max_lifetime': 0
    },
    'sim': {
        'min_ops_per_transaction': 0,
        'max_ops_per_transaction': 0,
        'delay_rate': 0.0,
        'standard_read_time': 0,
        'standard_write_time': 0,
        'realtime_read_time': 0,
        'realtime_write_time': 0,
        'simulation_time': 0
    }
}

# This describes the excepted format of every parameter, as well as some
# validity check for certain parameters.
fmt = {
    'db': {
        'standard_items': (int, lambda s, v: v > 0),
        'realtime_items': (int, lambda s, v: v > 0),
        'items_min_lifetime': (
            int, lambda s, v: 0 < v <= s['db']['items_max_lifetime']
        ),
        'items_max_lifetime': (
            int, lambda s, v: s['db']['items_max_lifetime'] <= v
        )
    },
    'sim': {
        'min_ops_per_transaction': (
            int, lambda s, v: 0 < v <= s['sim']['min_ops_per_transaction']
        ),
        'max_ops_per_transaction': (
            int, lambda s, v: s['sim']['min_ops_per_transaction'] <= v
        ),
        'delay_rate': (float, lambda s, v: v > 0),
        'standard_read_time':  (int, lambda s, v: v > 0),
        'standard_write_time': (int, lambda s, v: v > 0),
        'realtime_read_time': (int, lambda s, v: v > 0),
        'realtime_write_time': (int, lambda s, v: v > 0),
        'simulation_time': (int, lambda s, v: v > 0)
    }
}


def update_from_file(filename):
    """
    Updates the settings structures based on an .INI file.
    :param filename: The path to the .INI file.
    """
    config = configparser.ConfigParser()
    config.read(filename)
    for section in config.sections():
        if section not in settings:
            raise SyntaxError("Invalid INI section: [%s]" % section)
        for key in config[section]:
            if key not in settings[section]:
                raise SyntaxError("Unexpected setting in section [%s]: %s." % (
                    section, key))
            settings[section][key] = config[section][key]

    enforce_types()
    check_config()


def enforce_types():
    """
    Enforces the correct types on all settings.
    """
    for section in fmt:
        for key in fmt[section]:
            fmt_func = fmt[section][key][0]
            try:
                settings[section][key] = fmt_func(settings[section][key])
            except ValueError as e:
                raise ValueError("Invalid type for setting %s in section "
                                 "[%s]." % (key, section))


def check_config():
    """
    Checks the validity of the settings dictionary as a whole. Should always be
    called before anything else starts.
    """
    enforce_types()

    for section in fmt:
        for key in fmt[section]:
            check = fmt[section][key][1]

            if not check(settings, settings[section][key]):
                raise ValueError("Invalid value for setting %s in section "
                                 "[%s]." % (key, section))
