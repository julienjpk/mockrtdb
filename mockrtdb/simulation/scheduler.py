# This file is part of mockrtdb.

# mockrtdb is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# mockrtdb is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# mockrtdb. If not, see <http://www.gnu.org/licenses/>.

from mockrtdb.simulation.transactions import Transaction
from mockrtdb.simulation.config import settings
from math import log
from random import random, choice


class Scheduler:
    """
    Represents the scheduler handling transactions.
    """

    def __init__(self):
        """
        Initialises a scheduler with an empty queue.
        """
        self.time = 1
        self.preemptions = None
        self.expiries = None
        self.log = None
        self.trs = []
        self.last = None
        self.reset_stats()

    @property
    def over(self):
        """
        :return: True if the scheduler's job is done (end time reached).
        """
        return self.time >= settings['sim']['simulation_time'] > 0

    def reset_stats(self):
        self.preemptions = {'std': 0, 'rt': 0}
        self.expiries = {'std': 0, 'rt': 0}
        self.log = {'std': 0, 'rt': 0}

    def summary(self):
        """
        class,number,preemptions,preemptions%,expiries,expiries%
        :return: A summary of everything's that's happened so far.
        """
        s = 'type,number,preemptions,expiries,expiries%%\n'
        s += 'standard,%d,%d,%d,%f\n'
        s += 'realtime,%d,%d,%d,%f\n'
        s += 'all,%d,%d,%d,%f'

        return s % (
            self.log['std'], self.preemptions['std'], self.expiries['std'],
            0 if self.log['std'] <= 0 else
            100 * self.expiries['std'] / self.log['std'],
            self.log['rt'], self.preemptions['rt'], self.expiries['rt'],
            0 if self.log['rt'] <= 0 else
            100 * self.expiries['rt'] / self.log['rt'],
            sum(self.log.values()), sum(self.preemptions.values()),
            sum(self.expiries.values()),
            100 * sum(self.expiries.values()) / sum(self.log.values())
        )

    def seed(self, db):
        """
        Seeds this scheduler's queue with the necessary update transactions for
        a database, and a set of user transactions spread out following a
        Poisson distribution.
        :param db: The associated database, possibly holding realtime items.
        """
        self.reset_stats()
        self.trs = Transaction.get_update_trs(db)
        self.log['rt'] = len(self.trs)

        end_date = settings['sim']['simulation_time']
        lambda_rate = settings['sim']['delay_rate']
        t = 1

        # Fill the timeline with transactions, each separated with a Poisson-
        # distributed delay. Stop when the cumulated minimal times of all
        # transactions exceeds the simulation time.
        while t < end_date:
            delay = int(-log(1 - random()) / lambda_rate)
            trs = Transaction(t + delay)
            trs.seed(db)
            self.trs.append(trs)
            t += delay + trs.min_execution_time

        # Remove whatever's overflowing. This could happen since the length of
        # each transaction isn't determined before the transaction is seeded
        # with operations.
        while self.trs[-1].ideal_end_time > end_date:
            self.trs.pop()

        self.log['std'] = len(self.trs) - self.log['rt']

    def tick(self):
        """
        This is the scheduler's core. This is where, at each step, a transaction
        is chosen for execution, and time is moved forward. The algorithm is as
        follows:

        1. Check preempted transactions: search their histories for ops that
           manipulated now updated data items. If such an op can be found,
           restart the whole transaction (clear the preempted flag and all locks
           that were set by the transaction).
        2. Cancel all transactions which couldn't execute at this point (now +
           min_execution_time > deadline). This will include expired
           transactions. Remove their old locks.
        3. Sort all transactions by deadline (earliest first) and arrival times
           (idem). Ignore completed transactions.
        4. Pick the first transaction which isn't blocked by a write lock. If no
           such transaction exists, don't do anything for this tick.
        5. Schedule the transaction if it isn't already, and give time to its
           current operation. If the transaction changes, set the preempted flag
           on the old one.
        """
        active = [t for t in self.trs if not t.cancelled and not t.done]

        for tr in [t for t in active if t.preempted and t.void]:
            tr.restart_at(self.time)

        for tr in [t for t in active if not t.could_run(self.time)]:
            self.expiries[tr.typecode] += 1
            tr.cancel()

        active = [t for t in self.trs if not t.cancelled and not t.done]
        ready = sorted([tr for tr in active
                        if tr.arrival <= self.time and not tr.blocked],
                       key=lambda t: (t.deadline, t.arrival))

        eligible = [tr for tr in ready
                    if tr.arrival == ready[0].arrival
                    and tr.deadline == ready[0].deadline]

        self.time += 1

        if len(eligible) > 0:
            next_tr = self.last if self.last in eligible else choice(eligible)
            if self.last is not None and next_tr is not self.last:
                self.last.preempted = True
                self.preemptions[next_tr.typecode] += 1

            next_tr.work(self.time)

            self.last = None if next_tr.done else next_tr

    def __repr__(self):
        """
        Formats the scheduler's queue as a string for debug purposes.
        """
        return "Time: %d.\n%s" % (self.time, "\n".join(
            ("- " + t.__repr__()) for t in self.trs))
