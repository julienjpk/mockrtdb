# This file is part of mockrtdb.

# mockrtdb is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# mockrtdb is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# mockrtdb. If not, see <http://www.gnu.org/licenses/>.

from mockrtdb.simulation.database import Database
from mockrtdb.simulation.config import settings
from random import random, randint
from math import ceil


class Operation:
    """
    Represents a single operation in a transaction.
    """

    READ = 0
    WRITE = 1

    def __init__(self, tr, op, item):
        """
        Initialises the operation.
        :param tr: The associated transaction.
        :param op: The type of the operation (READ/WRITE).
        :param item: The data item associated with the operation.
        """
        self.tr = tr
        self.op = op
        self.item = item
        self.entry = 1
        self.progress = 0

    @property
    def done(self):
        """
        :return: True when the op has completed.
        """
        return self.progress >= self.min_execution_time

    @property
    def min_execution_time(self):
        """
        :return The minimum execution time required for the operation (if not
        preempted).
        """
        return self.item.read_time \
            if self.op == Operation.READ \
            else self.item.write_time

    @property
    def deadline(self):
        """
        :return: The operation's deadline, which is the same as its data item's
        expiration date (standard items don't have one).
        """
        return self.item.expiration_date

    @property
    def void(self):
        """
        :return: True if the operation is void (its item has been modified since
        it was scheduled). This is meaningless for completed transactions.
        """
        return self.entry < self.item.ts and self.item.ts_by is not self

    @property
    def blocked(self):
        """
        :return: True if the op's blocked by another's write lock.
        """
        ignore = self.done or self.op != Operation.WRITE
        return not ignore and not self.item.access(self)

    def release_lock(self):
        """
        Releases the lock on the item, if it's allowed for this operation.
        """
        if not self.item.access(self):
            return False
        self.item.write_lock = None
        return True

    def reset(self):
        """
        Resets the entire operation, releases the lock if possible.
        """
        self.progress = 0
        self.entry = 1
        self.release_lock()

    def work(self, t):
        """
        Gives some scheduling time to the operation. Clears the item lock when
        done.
        """
        if not self.done and not self.blocked:
            if self.entry <= 1:
                self.entry = t
            self.progress += 1
            if self.op == Operation.WRITE:
                self.item.write_lock = self
                if self.done:
                    self.item.ts = t
                    self.item.ts_by = self
                    self.release_lock()

    def __repr__(self):
        """
        Formats the operation as a string for debug purposes.
        """
        op_type = "Read" if self.op == Operation.READ else "Write"
        return "(%s on %d entry at %d, %d/%d, until %d)" % (
            op_type, self.item.id, self.entry,
            self.progress, self.min_execution_time, self.deadline
        )


class Transaction:
    """
    Represents a transaction, a sequence of operations.
    """

    LAST_ID = 1

    def __init__(self, arrival):
        """
        Creates a transaction.
        :param arrival: The transaction's request time.
        (0 for one shot operations ; this is for realtime update transactions)
        """
        self.id = Transaction.LAST_ID
        self.arrival = arrival
        self.deadline_salt = ceil(random() + 2)
        self.ops = []
        self.op = 0
        self.preempted = False
        self.cancelled = False
        Transaction.LAST_ID += 1

    @property
    def ongoing(self):
        """
        :return: True if this transaction has begun and done some work.
        """
        return self.op > 0 or self.ops[self.op].progress > 0

    @property
    def done(self):
        """
        :return: True when the transaction has completed.
        """
        return self.cancelled or all(op.done for op in self.ops)

    @property
    def min_execution_time(self):
        """
        :return The minimum execution time required by the transaction: the sum
        of the minimum times of all its operations.
        """
        return sum(op.min_execution_time for op in self.ops)

    @property
    def ideal_end_time(self):
        """
        :return: The time at which this transaction will complete if it is
        immediately scheduled and never preempted.
        """
        return self.arrival + self.min_execution_time

    @property
    def deadline(self):
        """
        :return: The transaction's deadline: the shortest deadlines among its
        operations.
        """
        if self.realtime:
            d = self.arrival + min(op.item.lifetime for op in self.ops)
        else:
            d = self.arrival + self.deadline_salt * self.min_execution_time
        return min(d, settings['sim']['simulation_time'])

    @property
    def realtime(self):
        """
        :return: True if any of this transaction's ops involves updating a
        realtime item.
        """
        return any(op.item.realtime and op.op == Operation.WRITE
                   for op in self.ops)

    @property
    def progress(self):
        return sum(op.progress for op in self.ops)

    @property
    def blocked(self):
        """
        :return: True if the transaction's current op is blocked by a lock.
        """
        return not self.done and self.ops[self.op].blocked

    @property
    def void(self):
        """
        :return: True if the transaction has been voided by an item update
        during its execution.
        """
        return self.ongoing and any(op.void for op in self.ops)

    @property
    def typecode(self):
        return 'rt' if self.realtime else 'std'

    @staticmethod
    def get_update_trs(db: Database):
        """
        Builds a set of transactions responsible for the updating of all
        realtime data items in a database.
        :param db: The associated database.
        :return: The set of associated realtime updates transactions.
        """
        update_trs = []
        for item in db.rts:
            period = ceil(2 * item.lifetime / 3)
            for entry in range(period, settings['sim']['simulation_time'],
                               period):
                tr = Transaction(entry)
                tr.ops.append(Operation(tr, Operation.WRITE, item))
                update_trs.append(tr)
        return update_trs

    def could_run(self, t):
        """
        :param t: A reference time.
        :return: True is this transaction could complete in time if it were
        scheduled at time t and never preempted.
        """
        return t < self.arrival or t + self.min_execution_time < self.deadline

    def seed(self, db: Database):
        """
        Seeds a (user) transaction with a set of random operations.
        :param db: A database from which to pick data items.
        """
        n_ops = randint(settings['sim']['min_ops_per_transaction'],
                        settings['sim']['max_ops_per_transaction'])

        for i in range(n_ops):
            p = random()
            if p < 1/3:
                self.ops.append(Operation(self, Operation.READ, db.pick(True)))
            elif p <= 2/3:
                self.ops.append(Operation(self, Operation.READ, db.pick()))
            else:
                self.ops.append(Operation(self, Operation.WRITE, db.pick()))

    def work(self, t):
        """
        Allows the transaction's current operation to take place for a bit.
        """
        if not self.done:
            self.ops[self.op].work(t)
            if self.ops[self.op].done:
                self.op += 1

    def restart_at(self, time):
        """
        Resets all ops in the current operation and set a new arrival time.
        """
        self.arrival = time
        self.op = 0
        self.preempted = False

        for op in self.ops:
            op.progress = 0
            op.release_lock()

    def cancel(self):
        """
        Cancels a transaction completely.
        """
        self.cancelled = True
        for op in self.ops:
            op.release_lock()

    def __repr__(self):
        """
        Formats the transaction as a string for debug purposes.
        """
        active = "cancelled" if self.cancelled else "queued"
        state = "preempted" if self.preempted else "on schedule"
        blocked = "blocked" if self.blocked else "clear"

        return "#%d, arrives at %d until %d, %s, %s, %s => %s" % (
            self.id, self.arrival, self.deadline, active, state, blocked,
            ', '.join(op.__repr__() for op in self.ops)
        )
