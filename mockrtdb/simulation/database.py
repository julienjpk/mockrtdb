# This file is part of mockrtdb.

# mockrtdb is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# mockrtdb is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# mockrtdb. If not, see <http://www.gnu.org/licenses/>.

from mockrtdb.simulation.config import settings
from random import randint, random, choice


class DataItem:
    """
    Represents a single standard (non-realtime) data item.
    """
    LAST_ID = 0

    def __init__(self):
        """
        Initialises the item with an ID and a random float value.
        """
        DataItem.LAST_ID += 1

        self.id = DataItem.LAST_ID
        self.value = random()
        self.ts = 1
        self.ts_by = None
        self.write_lock = None

    @property
    def expiration_date(self):
        """
        :return: The expiration date of the item, which in this case is the end
        of the simulation.
        """
        return settings['sim']['simulation_time']

    @property
    def read_time(self):
        """
        :return: The time it takes to read this item.
        """
        return settings['sim']['standard_read_time']

    @property
    def write_time(self):
        """
        :return: The time it takes to update this item.
        """
        return settings['sim']['standard_write_time']

    @property
    def realtime(self):
        """
        :return: False ; this is the base standard item class.
        """
        return False

    def access(self, op):
        """
        :param op: A reference operation.
        :return: False if the operation's blocked by the item's lock.
        """
        return any(self.write_lock is e for e in (None, op))

    def __repr__(self):
        """
        Formats the data item as a string for debug purposes.
        """
        return "(%d at %d, %s)" % (
            self.id, self.ts, "LK" if self.write_lock else "UN")


class RTDataItem(DataItem):
    """
    Represents a single realtime data item.
    """

    def __init__(self):
        """
        Initialises the item, picking a lifetime values based on our settings.
        """
        DataItem.__init__(self)
        self.lifetime = randint(settings['db']['items_min_lifetime'],
                                settings['db']['items_max_lifetime'])

    @property
    def expiration_date(self):
        """
        :return: The expiration time for this realtime item, which is its last
        update time plus its lifetime.
        """
        return min(settings['sim']['simulation_time'], self.ts + self.lifetime)

    @property
    def read_time(self):
        """
        :return: The time it takes to read this item.
        """
        return settings['sim']['realtime_read_time']

    @property
    def write_time(self):
        """
        :return: The time it takes to update this item.
        """
        return settings['sim']['realtime_write_time']

    @property
    def realtime(self):
        """
        :return: True ; this is the realtime item class.
        """
        return True

    def __repr__(self):
        """
        Formats the data item as a string for debug purposes.
        """
        return "(%d at %d for %d, %s)" % (
            self.id, self.ts, self.lifetime,
            "UN" if self.write_lock is None else ("LK" + repr(self.write_lock)))


class Database:
    """
    Represents a set of data items of both types.
    """

    def __init__(self):
        """
        Creates an empty database.
        """
        self.capacity = 0
        self.stds = []
        self.rts = []

    def seed(self):
        """
        Seeds the database by creating random items based on the settings.
        """
        self.capacity = sum(settings['db'][k + '_items']
                            for k in ('standard', 'realtime'))
        self.stds = [DataItem()
                     for _ in range(settings['db']['standard_items'])]
        self.rts = [RTDataItem()
                    for _ in range(settings['db']['realtime_items'])]

    def pick(self, realtime=False):
        """
        Returns a random data item from this database.
        :param realtime: Whether the item should be realtime or not.
        :return: A random item.
        """
        collection = self.rts if realtime else self.stds
        return choice(collection)

    def __repr__(self):
        """
        Formats the database as a string for debug purposes.
        """
        return "%s\n%s" % (
            ', '.join(i.__repr__() for i in self.stds),
            ', '.join(i.__repr__() for i in self.rts)
        )
