#!/usr/bin/env python3

# This file is part of mockrtdb.

# mockrtdb is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# mockrtdb is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# mockrtdb. If not, see <http://www.gnu.org/licenses/>.

from mockrtdb.simulation.config import update_from_file, check_config
from mockrtdb.simulation.database import Database
from mockrtdb.simulation.scheduler import Scheduler
from mockrtdb.interactive.app import AppView

from argparse import ArgumentParser
import sys


def main():
    # Command-line parser. This allows you to either select a configuration file
    # or give all parameters directly. When no command-line arguments are given,
    # the program goes into interactive mode and starts the ncurses interface.
    parser = ArgumentParser()
    parser.add_argument("-c", dest='config_file')
    args = parser.parse_args()
    interactive = True

    # Configuration file passed, try and parse it.
    if args.config_file is not None:
        try:
            update_from_file(args.config_file)
            interactive = False
        except SyntaxError as e:
            print("An error occured while parsing %s.\n%s" % (
                args.config_file, str(e)))
            sys.exit(1)

    # Parameters given directly on the command line, check the configuration.
    # NOTE: this isn't currently implemented.
    elif len(vars(args)) > 1:
        try:
            check_config()
            interactive = False
        except ValueError as e:
            print("An error occured while checking your command-line "
                  "configuration.\n%s" % str(e))
            sys.exit(1)

    if interactive:
        view = AppView()
        view.mainloop()
    else:
        # Quick run mode: initialise everything now.
        db = Database()
        sched = Scheduler()
        db.seed()
        sched.seed(db)

        # Tick-tock goes the clock...
        while not sched.over:
            sched.tick()
        print(sched.summary())

if __name__ == '__main__':
    main()
