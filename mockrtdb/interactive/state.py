# This file is part of mockrtdb.

# mockrtdb is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# mockrtdb is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# mockrtdb. If not, see <http://www.gnu.org/licenses/>.

import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox


class TableView(tk.Frame):
    def __init__(self, state, title, headers):
        """
        Convenience (base) class for table views (DB and sched. states).
        :param state: A reference to the main state view.
        :param title: The table's heading.
        :param headers: The table's headers.
        """
        tk.Frame.__init__(self, state)

        tk.Label(self, text=title, anchor='w', font=('', 12),
                 relief=tk.RIDGE).grid(ipady=5, sticky='WE')

        self.app = state.app
        self.table = ttk.Treeview(self, show="headings")
        self.table.grid(row=1, column=0, sticky='NSWE')

        self.table.configure(columns=list(h[0] for h in headers))
        for col_id, title, width in headers:
            self.table.column(col_id, width=width)
            self.table.heading(col_id, text=title)

        self.grid_rowconfigure(0, weight=0)
        self.grid_rowconfigure(1, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.table.bind('<Double-1>', self.item_view)

    def item_view(self, event):
        """
        Reacts to double clicks.
        :param event: The Tk event.
        """
        raise NotImplementedError('item_view not implemented in ' %
                                  self.__class__.__name__)


class DatabaseView(TableView):
    def __init__(self, state):
        """
        Initialises the database view.
        :param state: A reference to the main state view.
        """
        TableView.__init__(self, state, 'Data items', (
            ('id', 'ID', 40),
            ('value', 'Value', 100),
            ('ts', 'Timestamp', 100),
            ('ts_by', 'Updated by', 100),
            ('write_lock', 'Locked', 80),
            ('lifetime', 'Lifetime', 80)
        ))

    def update(self):
        """
        Updates the database view after every scheduler tick.
        """
        if self.app.db is None:
            return

        self.table.delete(*self.table.get_children())
        for item in self.app.db.stds:
            ts_by = "-" if item.ts_by is None else item.ts_by.tr.id
            lock = "-" if item.write_lock is None else item.write_lock.tr.id
            self.table.insert('', 'end', values=(
                item.id, item.value, item.ts, ts_by, lock))
        for item in self.app.db.rts:
            ts_by = "-" if item.ts_by is None else item.ts_by.tr.id
            lock = "-" if item.write_lock is None else item.write_lock.tr.id
            self.table.insert('', 'end', values=(
                item.id, item.value, item.ts, ts_by, lock, item.lifetime))

    def item_view(self, event):
        """
        Gives more information about a data item.
        :param event: The Tk event.
        """
        item = self.table.item(self.table.selection()[0], "values")
        db_item = [i for i in (self.app.db.stds + self.app.db.rts)
                   if i.id == int(item[0])]

        if len(db_item) <= 0:
            return

        tk.messagebox.showinfo('DB item #%d' % db_item[0].id, repr(db_item[0]))


class SchedView(TableView):
    def __init__(self, state):
        """
        Initialises the scheduler view.
        :param state: A reference to the main state view.
        """
        TableView.__init__(self, state, 'Scheduler', (
            ('id', 'ID', 15),
            ('arrival', 'Arrives at', 40),
            ('progress', 'Progress', 40),
            ('deadline', 'Deadline', 40),
            ('preempted', 'Preempted', 40),
            ('cancelled', 'Cancelled', 40)
        ))

    def update(self):
        """
        Updates the scheduler view after every scheduler tick.
        """
        if self.app.sched is None:
            return

        self.table.delete(*self.table.get_children())
        for tr in self.app.sched.trs:
            progress = "%d/%d" % (tr.progress, tr.min_execution_time)
            self.table.insert('', 'end', values=(
                tr.id, tr.arrival, progress, tr.deadline, tr.preempted,
                tr.cancelled))

    def item_view(self, event):
        """
        Gives more information about a transaction.
        :param event: The Tk event.
        """
        item = self.table.item(self.table.selection()[0], "values")
        trs = [t for t in self.app.sched.trs if t.id == int(item[0])]

        if len(trs) <= 0:
            return

        tk.messagebox.showinfo('Transaction #%d' % trs[0].id, repr(trs[0]))


class StateView(tk.Frame):
    def __init__(self, app):
        """
        Main state view: holds both tables and links them to the model.
        :param app: The main view (AppView).
        """
        tk.Frame.__init__(self, app.master)
        self.app = app

        paned = tk.PanedWindow(self, orient=tk.VERTICAL, sashpad=5,
                               sashrelief=tk.RAISED)
        paned.pack(fill=tk.BOTH, expand=True)

        self.db = DatabaseView(self)
        self.sched = SchedView(self)
        paned.add(self.db)
        paned.add(self.sched)

    def update(self):
        """
        Propagates an update request to the subcomponents.
        """
        self.db.update()
        self.sched.update()
