# This file is part of mockrtdb.

# mockrtdb is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# mockrtdb is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# mockrtdb. If not, see <http://www.gnu.org/licenses/>.

import tkinter as tk

from mockrtdb.simulation.database import Database
from mockrtdb.simulation.scheduler import Scheduler
from mockrtdb.interactive.setup import SetupView
from mockrtdb.interactive.state import StateView


class AppView(tk.Frame):
    def __init__(self):
        """
        Initialises the main graphical component (and the Tk master).
        """
        tk.Frame.__init__(self, tk.Tk())
        self.db = self.sched = self.state = None

        self.master.wm_title('mockrtdb - interactive mode')
        self.setup_components()

    def setup_components(self):
        """
        Sets up the 2 subcomponents : setup and state.
        """
        paned = tk.PanedWindow(self.master, orient=tk.HORIZONTAL,
                               sashrelief=tk.RAISED, sashpad=5)
        paned.pack(fill=tk.BOTH, expand=True)

        self.state = StateView(self)
        paned.add(SetupView(self))
        paned.add(self.state)

    def init_simulation(self):
        """
        Initialises a new simulation by creating/seeding a new database and a
        new scheduler.
        """
        self.db = Database()
        self.sched = Scheduler()
        self.db.seed()
        self.sched.seed(self.db)
