# This file is part of mockrtdb.

# mockrtdb is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# mockrtdb is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# mockrtdb. If not, see <http://www.gnu.org/licenses/>.

import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox

from mockrtdb.simulation.config import settings, check_config


class SetupView(tk.Frame):
    def __init__(self, app):
        """
        Initialises the setup view (left side).
        :param app: The main view.
        """
        tk.Frame.__init__(self, app.master)
        self.app = app
        self.form = {}

        structure = (
            ('db', 'Database settings', (
                ('Standard items', ('standard_items',)),
                ('Realtime items', ('realtime_items',)),
                ('Items lifetime range', ('items_min_lifetime',
                                          'items_max_lifetime'))
            )),
            ('sim', 'Simulation settings', (
                ('Ops. per transaction', ('min_ops_per_transaction',
                                          'max_ops_per_transaction')),
                ('Delay rate', ('delay_rate',)),
                ('Standard read/write times', ('standard_read_time',
                                               'standard_write_time')),
                ('Realtime read/write times', ('realtime_read_time',
                                               'realtime_write_time')),
                ('Simulation time', ('simulation_time',))
            ))
        )

        tk.Label(self, text="mockrtdb", font=('', 20)).pack(fill=tk.X, pady=30)

        self.build_structure(structure)

        buttons = tk.Frame(self)

        self.start = tk.Button(buttons, text='Start simulation',
                               command=self.on_start)
        self.tick = tk.Button(buttons, text='Next iteration', state=tk.DISABLED,
                              command=self.tick)
        self.start.pack(side=tk.LEFT, expand=True, fill=tk.X, anchor='n')
        self.tick.pack(side=tk.LEFT, expand=True, fill=tk.X, anchor='n')
        buttons.pack(expand=False, fill=tk.Y, anchor='n')

        self.log = tk.Text(self, state=tk.DISABLED, width=50, height=10)
        self.log.pack(side=tk.BOTTOM, expand=False)

    def on_start(self):
        """
        Callback for the start button.
        """
        for group, fields in self.form.items():
            for setting, entry in fields.items():
                settings[group][setting] = entry.get()

        try:
            check_config()
        except ValueError as e:
            tk.messagebox.showerror('Configuration error', str(e))
            return

        self.app.init_simulation()
        self.app.state.update()
        self.log.configure(state=tk.NORMAL)
        self.log.delete('1.0', 'end')
        self.log.configure(state=tk.DISABLED)
        self.tick.configure(state=tk.ACTIVE)

    def tick(self):
        """
        Callback for the next button.
        """
        if self.app.sched.over:
            self.tick.configure(state=tk.DISABLED)
            self.log.configure(state=tk.NORMAL)
            self.log.delete('1.0', 'end')
            self.log.insert('end', self.app.sched.summary())
            self.log.configure(state=tk.DISABLED)
            return

        self.app.sched.tick()
        self.app.state.update()

    def build_structure(self, structure):
        """
        Convenience method for building forms in tabs.
        :param structure: A form description.
        """
        nb = ttk.Notebook(self)

        for i in range(len(structure)):
            group, heading, rows = structure[i]
            width = max(len(f) for _, f in rows)

            frame = tk.Frame(nb)
            nb.add(frame, text=heading, sticky='NSWE')
            frame.grid_columnconfigure(0, weight=1)
            frame.grid_columnconfigure(1, weight=1)

            self.form[group] = {}
            for j in range(len(rows)):
                label_text, fields = rows[j]
                label = tk.Label(frame, text=label_text)
                label.grid(row=2*j, column=0, columnspan=width)

                for k in range(len(fields)):
                    self.form[group][fields[k]] = tk.Entry(frame)
                    self.form[group][fields[k]].grid(
                        row=2*j+1, column=k, sticky='WE',
                        columnspan=int(width / len(fields)))

        nb.pack(fill=tk.X)
