#!/usr/bin/env python3

# This file is part of mockrtdb.

# mockrtdb is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# mockrtdb is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# mockrtdb. If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

setup(
    name='mockrtdb',
    version='1.0.0.dev1',
    description='A Python simulator of a database management system involving '
                'real-time elements.',
    url='https://github.com/julienjpk/mockrtdb',
    author='Julien JPK',
    author_email='julienjpk@email.com',
    license='GPL-3.0',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Environment :: Console',
        'Environment :: Console :: Curses',
        'Intended Audience :: Education',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Database',
        'Topic :: Education :: Testing',
        'Topic :: Scientific/Engineering',
    ],
    keywords='real-time database simulation simulator',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'mockrtdb = mockrtdb.__main__:main'
        ]
    }
)
