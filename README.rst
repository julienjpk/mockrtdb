mockrtdb
========

.. image:: https://img.shields.io/github/license/julienjpk/mockrtdb.svg
   :target: https://raw.githubusercontent.com/julienjpk/mockrtdb/master/LICENSE

mockrtdb is a simple Python simulator for a real-time database management
system. This is being developed for educational purposes and shouldn't be used
in more serious contexts without some reviewing *(not that education isn't
serious... I mean... You know.)*.

Installing
----------

I've done my best to use `setuptools` correctly, and make this program easy to
install as a Python module. If everything's okay, you should be able to install
the application using::

  ./setup.py install

This should create a `mockrtdb` executable in your `PATH`, probably at
`/usr/local/bin/mockrtdb` (**conclusion:** by default, the install script
requires root permission).

If you don't trust me *(no hard feelings)*, there's a way to run that thing
without installing it (see below).

Running
-------

If you've installed it, mockrtdb can be run with::

  $ mockrtdb

If you encounter permission errors, ensure that:

- You can read mockrtdb's `.egg` file in your Python packages directory. You can
  get that with::

    python3 -c 'import site; print(site.getsitepackages())'

- You can execute the executable script created in your `PATH`. Go for::

    which mockrtdb

Beyond that, I'm gonna assume something's fishy with your setup. *Or it could be
my packaging, but let's no- shhhh no, let's not go there.*

If you're feeling adventurous, you might want to try and run it without
installing. Note that mockrtdb doesn't have any dependency beyond what Python 3
offers natively, so the following *shouldn't* cause problems::

  $ PYTHONPATH=. python3 mockrtdb/__main__.py

Or if, like me, you have a Python Docker image laying around::

  $ docker run -tiv $PWD:/opt/mockrtdb python /bin/bash
  # cd /opt/mockrtdb
  # ./setup.py install
  # mockrtdb

Anyway, once you've managed to start it, you can use it in two differents ways:

1. Interactively, passing no command-line arguments at all.
2. Using a configuration file (`.ini`) and passing it with the `-c` switch.

The last one will give a quick result : you'll get the final summary directly to
standard output. Option 1 gives you a nice little Tk interface to play with, and
you can control the simulation step by step.

Usage
-----

Interactive interface
~~~~~~~~~~~~~~~~~~~~~

When it is run without any arguments, `mockrtdb` starts its interactive
interface.

.. image:: https://cdn.pbrd.co/images/AlQcwM2HG.png

Since I really *hate* building GUIs, that thing couldn't be more
straightforward. Simply input the same simulation parameters as you would in a
file (more details below) and hit *Start simulation*. If everything's okay, the
*Next iteration* button will light up, and you'll be able to see the state of
the simulation on the right hand side.

Configuration files
~~~~~~~~~~~~~~~~~~~

`mockrtdb` can also be run quickly with a configuration file, using the `-c`
switch. The file is expected to be in *INI* format, and to contain two
sections : `[db]` and `[sim]`. Here's a sample::

  [db]
  standard_items =           # The number of standard items to generate.
  realtime_items =           # The number of realtime items to generate.
  items_min_lifetime =       # The minimum lifetime of a realtime item.
  items_max_lifetime =       # The maximum lifetime of a realtime item.

  [sim]
  min_ops_per_transaction =  # The minimum number of operations per transaction.
  max_ops_per_transaction =  # The maximum number of operations per transaction.
  delay_rate =               # The delay rate in-between two user transactions.
  standard_read_time =       # The read time on a standard item.
  standard_write_time =      # The write time on a standard item.
  realtime_read_time =       # The read time on a realtime item.
  realtime_write_time =      # The write time on a realtime item.
  simulation_time =          # The simulation time (maximum number of ticks).

With this method, the scheduler will tick without interruption until it reaches
`simulation_time`. The program will then print a summary and exit.

Design
------

The configuration settings will allow the program to initialise a collection of
data items (database) on which to work. Once this is done, the scheduler may be
initialised and seeded with the items' update transactions, and
Poisson-distributed user transactions. At this point, all that's left to do is
to "tick the clock" step by step until we reach the maximum simulation time. At
each step, the following algorithm applies (note that completed and cancelled
transactions are to be ignored) :

1. Check all preempted transactions, and search their histories for operations
   the data items of which have been updated during their preemption time. If
   such an operation can be found, restart the entire transaction. This will
   clear the preempted flag, and release all locks owned by the transaction.

2. Cancel all transactions that simply couldn't be completed in time (their
   minimum execution goes beyond their deadlines). This is a simple way to deal
   with expired transactions while at the same time pruning some in advance.

3. Sort all transactions by deadline (earliest first) and arrival times (same)
   when needed.

4. Pick the first transaction which isn't being blocked by a write lock. If no
   such transaction exists, the scheduler will not be doing any work this tick.

5. Schedule the selected transaction if it isn't already, and grant some
   execution time to its current operation. Set the preempted flag on the
   previously scheduled transaction if appropriate.

Uninstalling
------------

First: if you've got an old version of `pip3` (hello, Ubuntu users), this is
probably not gonna work::

  # pip3 uninstall mockrtdb

If it doesn't, you can::

  # rm -i $(which mockrtdb)
  # rm -i <Your Python path>/dist-packages/*mockrtdb*

Again, for your Python path::

  python3 -c 'import site; print(site.getsitepackages())'
